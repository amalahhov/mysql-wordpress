resource "aws_instance" "mysql" {
  ami = lookup(var.AMIS, var.AWS_REGION)
  instance_type = "t3.micro"

  tags = {
      Name = "mysql"
  }

  provisioner "local-exec" {
  command = "sleep 60; ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook playbook.yml -i mysql_aws_ec2.yml -u ec2-user --private-key testkey --vault-password-file vaultpwd --tags mysql"
}

  # subnet
  # subnet_id = aws_subnet.main-private.id
  subnet_id = aws_subnet.main-public.id

  # SG
  vpc_security_group_ids = ["${aws_security_group.allow-ssh.id}"]

  # SSH
  key_name = aws_key_pair.testkeypair.key_name
}

resource "aws_instance" "wordpress" {
  ami = lookup(var.AMIS, var.AWS_REGION)
  instance_type = "t3.micro"

  tags = {
      Name = "wordpress"
  }

  provisioner "file" {
    source = "script.sh"
    destination = "/tmp/script.sh"
  }
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/script.sh",
      "sudo /tmp/script.sh",
      "sudo echo '${var.REMOTE_SSH}' >> /home/ec2-user/.ssh/authorized_keys"
    ]
  }

  connection {
    host = self.public_ip
    user = var.EC2_USER
    private_key = file("testkey")
  }

  provisioner "local-exec" {
  command = "sleep 90; ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook playbook.yml -i wordpress_aws_ec2.yml -u ec2-user --private-key testkey --vault-password-file vaultpwd --tags wordpress -e mysql_ip=${aws_instance.mysql.private_ip}"
}

  # subnet
  subnet_id = aws_subnet.main-public.id

  # SG
  vpc_security_group_ids = ["${aws_security_group.allow-world.id}"]

  # SSH
  key_name = aws_key_pair.testkeypair.key_name
}
