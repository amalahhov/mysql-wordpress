variable "AWS_REGION" {
  default = "eu-north-1"
}
variable "PATH_TO_PRIVATE_KEY" {
  default = "testkey"
}
variable "PATH_TO_PUBLIC_KEY" {
  default = "testkey.pub"
}
variable "AMIS" {
  type = map
  default = {
    eu-north-1 = "ami-0b7a46b4bd694e8a6"
  }
}

variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}
variable "EC2_USER" {
  default = "ec2-user"
}

variable "MY_IP" {}
variable "REMOTE_SSH" {}
variable "REMOTE_IP" {}
