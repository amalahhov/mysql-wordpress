# This is test assignment to spin up Wordpress and MySql DB in AWS  #

This project is using Ansible and terraform to spin up MySql database server and WordPress CRM server

## Prerequisites

In this project Ansible dynamic inventory plugins are in use. To work with plugins boto3 and botocore Python libraries must be installed:

```bash
pip install boto3
pip install botocore
```
## Usage

Open repository folder.

Change Ansible vault key and add new key to **vaultpwd** file and create **testkey** keypair.

Run Terraform commands:

```bash
terraform init
terraform apply
```
