resource "aws_key_pair" "testkeypair" {
  key_name = "testkeypair"
  public_key = file("${var.PATH_TO_PUBLIC_KEY}")
}
